variable "tenancy_ocid" {
    description = "OCID of the parent compartment"
    type        = string
}

variable "region" {
    # Required
    description = "home region"
    type        = string
}