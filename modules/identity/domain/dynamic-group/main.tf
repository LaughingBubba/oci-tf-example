# https://registry.terraform.io/providers/oracle/oci/latest/docs/resources/identity_dynamic_group
resource "oci_identity_dynamic_group" "dg" {
    # Required
    compartment_id = var.compartment_id
    description = var.description
    name = var.name
    matching_rule = var.matching_rule

    # Optional
    # defined_tags = {"Operations.CostCenter"= "42"}
    # freeform_tags = {"Department"= "Finance"}
}