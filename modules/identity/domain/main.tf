# https://registry.terraform.io/providers/oracle/oci/latest/docs/resources/identity_domain
resource "oci_identity_domain" "dmn" {
  # Required
  compartment_id = var.compartment_id
  description    = var.description
  display_name   = var.name
  home_region    = var.region
  license_type   = var.license_type

  # Optional
  # admin_email = var.domain_admin_email
  # admin_first_name = var.domain_admin_first_name
  # admin_last_name = var.domain_admin_last_name
  # admin_user_name = oci_identity_user.test_user.name
  # defined_tags = {"Operations.CostCenter"= "42"}
  # freeform_tags = {"Department"= "Finance"}
  # is_hidden_on_login = var.domain_is_hidden_on_login
  # is_notification_bypassed = var.domain_is_notification_bypassed
  # is_primary_email_required = var.domain_is_primary_email_required
}