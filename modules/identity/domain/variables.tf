variable "compartment_id" {
    # Required
    description = "OCID of the parent compartment"
    type        = string
}

variable "license_type" {
    # Required
    description = "Domain license type: Free/Premium/External etc"
    type        = string
    default     = "free"
}

variable "description" {
    # Required
    description = "Short one sentence description"
    type        = string
    default     = "Short one sentence description - please update this"
}

variable "name" {
    # Required
    description = "Display name for dashboard"
    type        = string
    default     = "Display name - please update this"
}

variable "region" {
    # Required
    description = "Domain home region"
    type        = string
}