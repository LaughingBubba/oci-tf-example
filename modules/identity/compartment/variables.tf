variable "parent_compartment" {
    description = "OCID of the parent compartment"
    type        = string
}

variable "description" {
    # Required
    description = "Short one sentence description"
    type        = string
    default     = "Short one sentence description - please update this"
}

variable "name" {
    # Required
    description = "Display name for dashboard"
    type        = string
    default     = "Display name - please update this"
}

variable "is_deleteable" {
    description = "Allows the compartment to be destroyed via TF"
    type        = bool
    default     = true
}