terraform {
  required_providers {
    oci = {
      source = "hashicorp/oci"
    }
  }
  backend "http" {
    update_method = "PUT"
    address       = "https://<<pre-authenticated-url>>/terraform.tfstate"
  }
}
/* Bucket & Pre-Authenticatd Request (par) for TF
Name:   par-terraform-expirydate-expirytime
        par-terraform-20231231-1224
URL:    https://<<pre-authenticated-url>>
Upload pre-existing when converting to remote backend for the 1st time: 
 curl https://<<pre-authenticated-url>> --upload-file terraform.tfstate
*/

locals {
  tenancy_ocid    = "ocid1.tenancy.oc1..<<id>>"
  tenancy_region  = "<<region>>"
  namespace       = "<<namespace>>"
  user_ocid        = "ocid1.user.oc1..<<id>>"
}

provider "oci" {
  tenancy_ocid     = local.tenancy_ocid
  region           = local.tenancy_region
  user_ocid        = local.user_ocid
  private_key_path = "~/.oci/<<name>>.pem"
  fingerprint      = "<<fingerprint>>"
}


// This should be created out of band so state never references it. This is here as FYI
resource "oci_objectstorage_bucket" "oci-terraform-state" {
    #Required
    compartment_id = local.tenancy_ocid
    name = "oci-terraform-state"
    namespace = local.namespace

    # Optional
    auto_tiering = "InfrequentAccess"
    freeform_tags = {"env"= "tenancy"}
    object_events_enabled = true
    versioning = "Enabled"
}

module "environments" {
    source = "./modules/environments"
    # input variables
    tenancy_ocid = local.tenancy_ocid
    region       = local.tenancy_region
}
